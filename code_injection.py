import struct
import ctypes
import platform
import intro_type

def tohex(s):
    return s.replace(' ', '').decode('hex')

class CodeInjector(object):
    """ Class that allow to inject native code and export it as a python object """ 
    get_obj_at = None

    # int-to-object payloads:
    #see my article at "blog.hakril.net/articles/1-understanding-python-by-breaking-it---lvl-2.html"

    # mov rax, [rsi + 0x24]
    # ret
    bootstrap_64 = tohex("48 8b 46 24 C3")
    # Real first argument is RDI

    # mov rax, id(None)
    # inc QWORD PTR [RAX]
    # ret
    return_none_64 = tohex("48 B8") + struct.pack("=Q", id(None))
    return_none_64 += tohex("48 FF 00 C3")

    # mov eax, [esp + 8]
    # mov eax, [eax + 14]
    # ret
    bootstrap_32 = tohex("8B 44 24 08 8B 40 14 C3")
    # Real first argument is [esp + 4]

    # mov eax, id(None)
    # inc DWORD PTR [EAX]
    # ret
    return_none_32 = tohex("B8") + struct.pack("=I", id(None))
    return_none_32 += tohex("FF 00 C3")


    bootstrap_code = {'32bit' : bootstrap_32, '64bit' : bootstrap_64}
    return_none = {'32bit' : return_none_32, '64bit' : return_none_64}
    obj_pack_string = {'32bit' : ("<IIII","<IIIII"), '64bit' : ("<QQQQ", "<QQQQQ")}
    int_size = {'32bit' : 4, '64bit' : 8}


    def __init__(self):
        self.maps = []
        self.get_new_page(0x1000)
        self.names = []
        if self.get_obj_at is None:
            self.bootstrap()

    @classmethod
    def get_bootstrap_payload(cls):
        bits = platform.architecture()[0]
        if bits not in cls.bootstrap_code:
            raise ValueError("Unknow platform bits <{0}>".format(bits))
        return cls.bootstrap_code[bits]

    @classmethod
    def get_pack_string(cls):
        bits = platform.architecture()[0]
        if bits not in cls.obj_pack_string:
            raise ValueError("Unknow platform bits <{0}>".format(bits))
        return cls.obj_pack_string[bits]

    @classmethod
    def get_int_size(cls):
        bits = platform.architecture()[0]
        if bits not in cls.int_size:
            raise ValueError("Unknow platform bits <{0}>".format(bits))
        return cls.int_size[bits]

    @classmethod
    def get_return_none(cls):
        bits = platform.architecture()[0]
        if bits not in cls.int_size:
            raise ValueError("Unknow platform bits <{0}>".format(bits))
        return cls.return_none[bits]

    def get_new_page(self, size):
        self.maps.append(intro_type.MyMap.get_map(size))
        self.cur_offset = 0
        self.cur_page_size = size

    def create_builtin(self, code, name="crafted builtin", builtin_self=0):
        #Check size of page
        payload_size = (len(code)+ ctypes.sizeof(intro_type.PyMethodDef) +
            ctypes.sizeof(intro_type.PyCFunctionObject))
        if payload_size + self.cur_offset > self.cur_page_size:
            self.get_new_page((payload_size + 0x1000) & ~0xfff)
        obj_pack_str = self.get_pack_string()
        offset = self.cur_offset
        mmap = self.maps[-1]
        #Get a C-string with the name
        name = ctypes.create_string_buffer(name)
        self.names.append(name)
        name_addr = ctypes.addressof(name)
        # write payload on the page
        mmap[offset : offset + len(code)] = code
        method_offset = offset + len(code)
        # write the PyMethodDef on the page
        # 8 is a flag value for builtin functions
        struct.pack_into(obj_pack_str[0], mmap, method_offset, name_addr, mmap.addr + offset, 8, name_addr)
        c_function_offset = method_offset + ctypes.sizeof(intro_type.PyMethodDef)
        # write the PyCFunctionObject on the page
        struct.pack_into(obj_pack_str[1],
            mmap, c_function_offset, 42, id(type(min)), mmap.addr + method_offset, builtin_self, 0)
        self.cur_offset = c_function_offset + ctypes.sizeof(intro_type.PyCFunctionObject)
        # Give life to our object
        return self.get_obj_at(struct.pack(obj_pack_str[0][0:2], int(mmap.addr + c_function_offset)))

    def bootstrap(self):
        payload = self.get_bootstrap_payload()
        old_abs = replace_builtin(abs, payload)
        self.__class__.get_obj_at = abs
        new_obj_at = self.create_builtin(payload, "get_obj_at")
        restore_builtins(abs, old_abs)
        self.__class__.get_obj_at = new_obj_at

    def reserve_int(self):
        int_size = self.get_int_size()
        if int_size + self.cur_offset > self.cur_page_size:
            self.get_new_page((payload_size + 0x1000) & ~0xfff)
        int_addr = self.maps[-1].addr + self.cur_offset
        self.cur_offset += int_size
        return int_addr

    def create_builtin_int_io(self, code, name):
        io_addr = self.reserve_int()
        io_clong = ctypes.c_long.from_address(io_addr)
        full_code = code + self.get_return_none()
        builtin = self.create_builtin(full_code, name, builtin_self=io_addr)
        print("IO Addr = {0}".format(hex(io_addr)))
        def builtin_wrapper(x):
            io_clong.value = x
            builtin(io_addr)
            return io_clong.value
        builtin_wrapper.__name__ = name + '_oiwrapper'
        return builtin_wrapper




def replace_builtin(b, code):
    bm = intro_type.PyCFunctionObject.from_address(id(b))
    map = intro_type.MyMap.get_map(0x1000)
    map[0:len(code)] = code 
    old_code = bm.m_ml.contents.ml_meth
    bm.m_ml.contents.ml_meth = map.addr
    return old_code

def restore_builtins(b, orig_code_addr):
    bm = intro_type.PyCFunctionObject.from_address(id(b))
    bm.m_ml.contents.ml_meth = orig_code_addr
