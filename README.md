# Native code execution in python.

This code play with ctypes in order to create custom `built-in function` allowing to simply execute native code in python.

### How does it work ?
Explanation are in this blog-post: [blog.hakril.net/articles/1-understanding-python-by-breaking-it---lvl-2.html](blog.hakril.net/articles/1-understanding-python-by-breaking-it---lvl-2.html)

### Files
`intro_type.py` is about `ctypes.Structure` and `mmap` wrapper.  
`code_injection.py` is the file with the principal class `CodeInjector`  
`demos.py` show how to use the `CodeInjector` class with one example for both 32 et 64bits python.

### Example

    :::python
    import code_injection
    ci = code_injection.CodeInjector()
    breakpoint = ci.create_builtin("\xcc", "breakpoint")
    >>> breakpoint
    <built-in function breakpoint>
    >>> breakpoint(0)
    Program received signal SIGTRAP, Trace/breakpoint trap.
    0x00007ffff7ff7001 in ?? ()
    (gdb)
