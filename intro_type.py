import ctypes
import mmap
import platform

# 'Instrospection' ctypes:
# Represent the C structs behind some python builtin objects
# to gain access to some internals values non exported into python
class PyObj(ctypes.Structure):
    _fields_ = [("ob_refcnt", ctypes.c_size_t),
                ("ob_type", ctypes.c_void_p)] #must be cast

class PyVarObj(PyObj):
    _fields_ = [("ob_size", ctypes.c_size_t)]


class PyMethodDef(ctypes.Structure):
    _fields_ = [("ml_name", ctypes.POINTER(ctypes.c_char)), ("ml_meth", ctypes.c_void_p),
            ("ml_flags", ctypes.c_int), ("ml_doc", ctypes.POINTER(ctypes.c_char))]

class PyCFunctionObject(PyObj):
    _fields_ = [("m_ml", ctypes.POINTER(PyMethodDef)),
        ("m_self", ctypes.c_void_p), ("m_module", ctypes.c_void_p)]


class PyMmap(PyObj):
    _fields_ = [("ob_addr", ctypes.c_size_t), ("ob_size", ctypes.c_size_t)]


# Specific mmap class for code injection
   
class MyMap(mmap.mmap):
    """ A mmap that is never unmapped and that contains the page address """
    def __init__(self, *args, **kwarg):
        #Get the page address by 'introspection' of the C struct
        m = PyMmap.from_address(id(self))
        self.addr = m.ob_addr
        #Prevent garbage collection (so unmaping) of the page
        m.ob_refcnt += 1

    @classmethod
    def get_map(cls, size):
        """ Dispatch to the good mmap implem depending on the current system """
        systems = {'windows' : Win32MyMap,
                    'linux' : UnixMyMap }
        x = platform.system().lower()
        if x not in systems:
            raise ValueError("Unknow system {0}".format(x))
        return systems[x].get_map(size)

class Win32MyMap(MyMap):
    @classmethod
    def get_map(cls, size):
        access = mmap.ACCESS_READ | mmap.ACCESS_WRITE
        return cls(-1, size, access=access)

class UnixMyMap(MyMap):
    @classmethod
    def get_map(cls, size):
        prot = mmap.PROT_EXEC | mmap.PROT_WRITE | mmap.PROT_READ
        return cls(-1, size, prot=prot)
