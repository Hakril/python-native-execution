import code_injection

ci = code_injection.CodeInjector()

breakpoint = ci.create_builtin("\xcc", "breakpoint")

# Simple demonstrations with identity functions
# Those even update the refcount of the objects!

# mov rax, rsi ; inc QWORD PTR [rax]; ret
payload_64 = "4889F048FF00C3".decode('hex')
identity_64 = ci.create_builtin(payload_64, "64_bits_identity")

# mov eax, [esp + 8]; inc DWORD PTR [eax]; ret
payload_32 = "8B442408FF00C3".decode('hex')
identity_32 = ci.create_builtin(payload_32, "32_bits_identity")

inc_int_64 = ci.create_builtin_int_io("\xff\x07", "x++")
inc_int_32 = ci.create_builtin_int_io("\x8B\x44\x24\x04\xFF\x00", "x++")
